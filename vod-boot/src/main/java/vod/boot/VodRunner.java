package vod.boot;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import vod.boot.repository.MovieRepository;

@Component
@RequiredArgsConstructor
@Slf4j
public class VodRunner implements CommandLineRunner {

    private final MovieRepository movieRepository;

    @Override
    public void run(String... args) throws Exception {
        log.info("there is {} movies in a repository", movieRepository.count());
    }
}

package vod.boot.web.dto;


import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;
import vod.boot.model.Director;
import vod.boot.model.Movie;


@Data
@NoArgsConstructor
public class MovieDto {

    private int id;
    @NotNull
    @Size(min=2,max=100)
    private String title;
    @NotNull
    private String poster;
    private int directorId;

    public MovieDto(Movie source) {
        this.id = source.getId();
        this.title = source.getTitle();
        this.poster = source.getPoster();
        this.directorId = source.getDirector().getId();
    }

    public Movie toData(){
        Movie data = new Movie();
        data.setId(this.id);
        data.setTitle(this.title);
        data.setPoster(this.poster);

        Director director = new Director();
        director.setId(this.directorId);
        data.setDirector(director);
        return data;
    }
}

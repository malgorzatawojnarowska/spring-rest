package vod.boot.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.boot.model.Movie;
import vod.boot.repository.MovieRepository;
import vod.boot.web.dto.MovieDto;

import java.net.URI;
import java.util.List;
import java.util.Locale;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
public class MoviesController {

    private final MovieRepository movieRepository;
    private final LocaleResolver localeResolver;
    private final MessageSource messageSource;

    @GetMapping("/movies")
    List<MovieDto> getMovies(){
        return movieRepository.findAll().stream().map(MovieDto::new).toList();
    }

    @GetMapping("/movies/{movieId}")
    MovieDto getMovie(@PathVariable int movieId){
        log.info("about to retrieve movie {}", movieId);
        return movieRepository.findById(movieId).map(MovieDto::new).orElse(null);
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(
            @Validated @RequestBody MovieDto dto,
            Errors errors,
            HttpServletRequest request){

        if (errors.hasErrors()) {
            Locale locale = localeResolver.resolveLocale(request);

            String message = errors.getAllErrors().stream()
                    .map(oe -> messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
                    .reduce("errors:\n", (accu, error) -> accu + error + "\n");

            return ResponseEntity.badRequest().body(message);
        }

        Movie movie = movieRepository.save(dto.toData());

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("{id}")
                .buildAndExpand(movie.getId())
                .toUri();
        return ResponseEntity.created(uri).body(new MovieDto(movie));
    }

}

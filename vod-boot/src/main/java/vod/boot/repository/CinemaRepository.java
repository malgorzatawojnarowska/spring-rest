package vod.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vod.boot.model.Cinema;

public interface CinemaRepository extends JpaRepository<Cinema, Integer> {
}

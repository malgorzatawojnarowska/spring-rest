package vod.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vod.boot.model.Rating;

public interface RatingRepository extends JpaRepository<Rating, Integer> {
}

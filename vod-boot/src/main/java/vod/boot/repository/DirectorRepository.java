package vod.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vod.boot.model.Director;

public interface DirectorRepository extends JpaRepository<Director, Integer> {
}

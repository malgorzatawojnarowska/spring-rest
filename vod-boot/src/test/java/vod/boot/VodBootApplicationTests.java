package vod.boot;

import io.restassured.RestAssured;
import io.restassured.matcher.RestAssuredMatchers;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class VodBootApplicationTests {

    @LocalServerPort
    private int port;

    @Test
    void contextLoads() {
        Response response = RestAssured
                .get("http://localhost:" + port + "/vod/webapi/movies/1");
        Map map = response.body().as(Map.class);
        Object id = map.get("id");

        Assertions.assertEquals(1, id);

        RestAssured
                .get("http://localhost:" + port + "/vod/webapi/movies/1")
                .then()
                .statusCode(200)
                .body("directorId", org.hamcrest.Matchers.equalTo(1));

    }

}

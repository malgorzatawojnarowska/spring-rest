package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class VodAdvice {

    private final MovieValidator movieValidator;
    private final RatingValidator ratingValidator;

    @InitBinder("ratingDto")
    void initBinderForRating(WebDataBinder binder) {
        binder.addValidators(ratingValidator);
    }


    @InitBinder("movieDto")
    void initBinderForMovie(WebDataBinder binder) {
        binder.addValidators(movieValidator);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    String handleIllegalArgumentException(IllegalArgumentException e) {
        log.error("illegal argument exception occurred", e);
        return e.getMessage();
    }
}

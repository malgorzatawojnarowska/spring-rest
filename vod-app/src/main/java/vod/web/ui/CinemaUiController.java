package vod.web.ui;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class CinemaUiController {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping("/cinemas")
    String getMapping(Model model, @RequestParam(value = "movieId", required = false) Integer movieId){

        List<Cinema> cinemas;
        String title;
        if(movieId!=null){
            Movie movie = movieService.getMovieById(movieId);
            cinemas = cinemaService.getCinemasByMovie(movie);
            title = "Cinemas showing " + movie.getTitle();
        } else {
            cinemas = cinemaService.getAllCinemas();
            title = "All cinemas";
        }

        model.addAttribute("cinemas", cinemas);
        model.addAttribute("title", title);

        return "/WEB-INF/jsp/cinemas.jsp";
    }

}

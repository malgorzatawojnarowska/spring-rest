package vod.web.dto;

import lombok.Data;
import vod.model.Cinema;

@Data
public class CinemaDto {
    private int id;
    private String name;
    private String logo;

    public static CinemaDto fromData(Cinema cinema){
        CinemaDto dto = new CinemaDto();
        dto.setId(cinema.getId());
        dto.setName(cinema.getName());
        dto.setLogo(cinema.getLogo());
        return dto;
    }
}

package vod.web.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;
import lombok.Data;
import vod.model.Rating;

@Data
public class RatingDto {

    private int id;
    @Min(value = 0)
    @Max(value = 5)
    private float rate;
    private int movieId;

    public static RatingDto fromData(Rating rating){
        RatingDto dto = new RatingDto();
        dto.id = rating.getId();
        dto.rate = rating.getRate();
        dto.movieId = rating.getMovie().getId();
        return dto;
    }
}

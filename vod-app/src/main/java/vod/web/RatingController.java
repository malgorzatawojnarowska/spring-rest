package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import vod.model.Movie;
import vod.model.Rating;
import vod.service.MovieService;
import vod.web.dto.RatingDto;

import java.util.List;
import java.util.Locale;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/webapi")
public class RatingController {

    private final MovieService movieService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;

    @GetMapping("/ratings")
    List<RatingDto> getRatings(@RequestParam("movieId") int movieId){
        log.info("about to retrieve ratings of a movie {}", movieId);
        Movie movie = movieService.getMovieById(movieId);

        return movieService.getRatingsByMovie(movie).stream()
                .map(RatingDto::fromData)
                .toList();
    }

    @PostMapping("/ratings")
    ResponseEntity<?> addRating(@Validated @RequestBody RatingDto ratingDto, Errors errors, HttpServletRequest request) {
        log.info("about to add rating {}", ratingDto);
        Locale locale = localeResolver.resolveLocale(request);

        if (errors.hasErrors()) {
            String message = errors.getAllErrors().stream()
                    .map(oe -> messageSource.getMessage(oe.getCode(), oe.getArguments(), locale) + "\n"
                    )
                    .reduce("errors:\n", (accu, error) -> accu + error);

            return ResponseEntity.badRequest().body(message);
        }

        Movie movie = movieService.getMovieById(ratingDto.getMovieId());
        Rating rating = movieService.rateMovie(movie,ratingDto.getRate());

        return ResponseEntity
                .status(HttpStatus.CREATED).body(RatingDto.fromData(rating));
    }



}

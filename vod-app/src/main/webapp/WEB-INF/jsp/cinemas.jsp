<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<jsp:include page="./header.jsp" flush="true"/>

<table>
    <tr>
        <th>Logo</th>
        <th>Name</th>
        <th>Movies</th>
    </tr>
    <c:forEach var="c" items="${cinemas}">
        <tr>
            <td>
                <img src="${c.logo}"/>
            </td>
            <td>
                    ${c.name}
            </td>
            <td class="search-container">
                <a class="search" href="./movies?cinemaId=${c.id}">&nbsp;</a>
            </td>
        </tr>
    </c:forEach>
</table>

<jsp:include page="./footer.jsp" flush="true"/>
